FROM gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:centos7

# Install dependencies
RUN yum install -y gcc-c++ wget diffstat chrpath socat xterm vim-common

# Create and configure vivado user
RUN useradd vivado
RUN passwd -f -u vivado
RUN usermod -aG wheel vivado
RUN echo "vivado ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

ARG  PETA_RUN_FILE
COPY accept-eula.sh ${PETA_RUN_FILE} /

# run the install
RUN chmod a+rx /${PETA_RUN_FILE} && \
  chmod a+rx /accept-eula.sh && \
  mkdir -p /opt/Xilinx && \
  chmod 777 /tmp /opt/Xilinx && \
  cd /tmp && \
  sudo -u vivado -i /accept-eula.sh /${PETA_RUN_FILE} /opt/Xilinx/petalinux && \
  rm -f /${PETA_RUN_FILE} /accept-eula.sh

USER vivado
ENV HOME /home/vivado
WORKDIR /home/vivado
RUN echo "source /opt/Xilinx/petalinux/settings.sh" >> /home/vivado/.bashrc
