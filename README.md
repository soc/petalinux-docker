# petalinux-docker

To build the docker image with petalinux installed, fork this repository and [download](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/embedded-design-tools.html) the petalinux installer to this folder. Then run:

    export PETALINUX_VERSION=<VERSION>
    docker login gitlab-registry.cern.ch
    docker build --build-arg PETA_RUN_FILE=petalinux-v${PETALINUX_VERSION}-final-installer.run -t gitlab-registry.cern.ch/<NAMESPACE>/petalinux-docker/petalinux-v${PETALINUX_VERSION} .
    docker push gitlab-registry.cern.ch/<NAMESPACE>/petalinux-docker

After installation, launch petalinux with:

`docker run -it --rm  gitlab-registry.cern.ch/soc/petalinux-docker/petalinux-v${PETALINUX_VERSION}`

Based on [https://github.com/z4yx/petalinux-docker](https://github.com/z4yx/petalinux-docker)
